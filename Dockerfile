﻿FROM golang:1.20.3-alpine AS builder

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /qkeruen

FROM builder as test
RUN go test -v ./...

FROM alpine:latest AS stage

WORKDIR /

COPY --from=builder /qkeruen /qkeruen

EXPOSE 3000

USER nobody

CMD ["/qkeruen"]